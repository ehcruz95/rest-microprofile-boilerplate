package rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Properties;

@Path("propriedades")
public class PropertiesResources {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Properties getSystemProperties() {
        return System.getProperties();
    }
}
