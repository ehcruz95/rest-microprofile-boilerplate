package rest.service;

import rest.domain.Usuario;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ApplicationScoped
public class UsuarioServiceImpl implements UsuarioService {

    private List<Usuario> usuarios = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void adicionarUsuario(Usuario usuario) {
        this.usuarios.add(usuario);
    }

    @Override
    public void removerUsuario(String email) {
        this.usuarios.remove(this.getUsuario(email));
    }

    @Override
    public void atualizarUsuario(String email, Usuario usuario) {
        Usuario user = this.getUsuario(email);
        int index = this.usuarios.indexOf(user);
        this.usuarios.get(index).setNome(usuario.getNome());
        this.usuarios.get(index).setEmail(usuario.getEmail());
    }

    @Override
    public Usuario getUsuario(String email) {
        for (Usuario usuario : this.usuarios) {
            if (usuario.getEmail().equals(email)) {
                return usuario;
            }
        }
        return null;
    }

    @Override
    public List<Usuario> getTodosUsuarios() {
        return new ArrayList<>(usuarios);
    }
}
