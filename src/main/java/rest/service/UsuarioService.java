package rest.service;

import rest.domain.Usuario;

import java.util.List;

public interface UsuarioService {

    void adicionarUsuario(Usuario usuario);

    void removerUsuario(String email);

    void atualizarUsuario(String email, Usuario usuario);

    Usuario getUsuario(String email);

    List<Usuario> getTodosUsuarios();

}
