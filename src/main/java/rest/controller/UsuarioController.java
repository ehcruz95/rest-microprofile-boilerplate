package rest.controller;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import rest.domain.Usuario;
import rest.service.UsuarioService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
@Path("usuario")
public class UsuarioController {

    @Inject
    private UsuarioService usuarioService;

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponse(
            responseCode = "200",
            description = "Lista de todos os usuários",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = Usuario.class
                    )
            )
    )
    public Response getTodosUsuarios() {
        List<Usuario> usuarios = this.usuarioService.getTodosUsuarios();
        return Response.ok(usuarios).build();
    }

    @POST
    @Path("salvar")
    @Consumes(MediaType.APPLICATION_JSON)
    public void salvar(Usuario usuario) {
        this.usuarioService.adicionarUsuario(usuario);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario getUsuarioByEmail(@QueryParam("email") String email) {
        return this.usuarioService.getUsuario(email);
    }

    @DELETE
    @Path("remover")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteUsario(@QueryParam("email") String email) {
        this.usuarioService.removerUsuario(email);
    }
}
