## REST Api com Microprofile #

Projeto boilerplate para documentação de api's REST usando Eclipse Microprofile.

#### Qual o propósito desse repositório?

* Mostrar conceitos básicos de criação de API Rest usando JAX-RS
* Documentar a API utilizando Eclipse Microprofile
* Entender melhor a configuração do Open Liberty